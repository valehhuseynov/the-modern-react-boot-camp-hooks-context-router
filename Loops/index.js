class App extends React.Component {
    render() {
        return (
            <div>
                <h1>Hello App</h1>
                <Friends 
                 name = "Valeh"
                 hobbies = {['Piano' , 'Swiming' , 'Dancing']}
                />

                <Friends 
                 name = "Murad"
                 hobbies = {['Driving' , 'Cooking']}
                />
            </div>
        )
    }
}

ReactDOM.render(<App /> , document.getElementById('root'));