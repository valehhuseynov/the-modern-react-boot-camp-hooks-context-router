class App extends React.Component {
    render() {
        return (
            <div className= "App">
                <Hello 
                  to = "Valeh"
                  from = "Baku"
                  bangs = {4}
                  img = "https://images.pexels.com/photos/555790/pexels-photo-555790.png?cs=srgb&dl=black-and-white-boy-casual-555790.jpg&fm=jpg"
                />
            </div>
        )
    }
}

ReactDOM.render(<App /> , document.getElementById("root"));