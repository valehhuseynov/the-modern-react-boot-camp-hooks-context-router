class Hello extends React.Component {
    render() {
        const props = this.props;
        let bangs = "!".repeat(props.bangs);
        return (
            <div>
                <p>- Hello {props.to}.</p>
                <p>- Where are you from?</p>
                <p>- I am from {props.from}</p>
                <p>- Thanks {bangs}</p>
                <img src = {props.img} style = {{width : "300px" , height : "300px"}} />
            </div>
        )
    }
}