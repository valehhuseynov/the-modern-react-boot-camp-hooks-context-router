class Hello extends React.Component {
    static defaultProps = {
        from : 'Anonymous',
        bangs : 1
    }
    render() {
        const {to , from , bangs} = this.props;
        const bang = "!".repeat(bangs);
        return (
            <div>
                <p>Hello {to} {from},How are you?{bang}</p>
            </div>
        )
    }
}