import React , { Component } from 'react';
import Dog from './Dog';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Dog />
        <p>I am another div , not in dog component</p>
      </div>
    )
  }
}

export default App;
