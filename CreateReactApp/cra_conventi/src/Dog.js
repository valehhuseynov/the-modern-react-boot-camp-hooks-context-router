/* eslint-disable jsx-a11y/alt-text */
import React , { Component } from "react";
import dog from "./dog.jpg";
import './Dog.css';
class Dog extends Component {
    render() {
        return (
            <div className="Dog">
                <h1>DOG!</h1>
                <img className="Dog-image" src = {dog} />
                
            </div>
        )
    }
}

export default Dog;