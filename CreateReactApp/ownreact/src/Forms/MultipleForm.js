import React, { Component } from 'react'

class MultipleForm extends Component {
    constructor(props) {
        super(props);
        this.state = {username: "", email: "", password: ""}
        this.handleChange = this.handleChange.bind(this);
        // this.hadleEmailChange = this.hadleEmailChange.bind(this);
        // this.hadlePasswordCHange = this.hadlePasswordCHange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleChange(evt) {
        this.setState({ [evt.target.name]: evt.target.value });
    }
    // hadleEmailChange(evt) {
    //     this.setState({email: evt.target.value});
    // }
    // hadlePasswordCHange(evt) {
    //     this.setState({passowrd: evt.target.value});
    // }
    handleSubmit(evt) {
        evt.preventDefault();
        alert(`You typed: ${this.state.username}`);
        this.setState({username: ""});
    }
    render() {
        return (
            <div>
                <h1>Form w/Multiple Inputs</h1>
                <form onSubmit={this.handleSubmit}>

                    <input 
                    type='text' 
                    name='username'
                    value={this.state.username} 
                    onChange={this.handleChange}
                    placeholder="username" 
                    />

                    <input 
                    type="email" 
                    name='email'
                    value={this.state.email}
                    onChange={this.handleChange}
                    placeholder="email" 
                    />

                    <input 
                    type="password" 
                    name='password'
                    value={this.state.passowrd}
                    onChange={this.handleChange}
                    placeholder="password" 
                    />

                    <button>Submit!</button>

                </form>
                
            </div>
        )
    }
}

export default MultipleForm;

