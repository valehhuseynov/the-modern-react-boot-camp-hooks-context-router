import React, { Component } from 'react'
import { Link } from "react-router-dom";
import Message from './Message';

class VendingMachine extends Component {
    render() {
        return (
            <div>
                <h1>Vending Machine</h1>
                <nav>
                    <Link to='/soda'>Soda</Link>
                    <Link to="/sardines">Sardines</Link>
                    <Link to="/chips">Chips</Link>
                    <Message />
                </nav>
                <img style={{width: '200px'}} src="https://sc01.alicdn.com/kf/UT8WWkgXZFaXXagOFbXt/205728887/UT8WWkgXZFaXXagOFbXt.jpg_.webp" alt="machine" />
            </div>
        )
    }
}

export default VendingMachine;