import React, { Component } from 'react'

class Sardines extends Component {
    render() {
        return (
            <div>
                <h1>Sardines</h1>
                <img style={{width: '200px'}} src="https://www.citarella.com/media/catalog/product/cache/1/mobile_image/9df78eab33525d08d6e5fb8d27136e95/w/i/wild_imported_sardines_mobile.jpg" alt="Sardines" />
            </div>
        )
    }
}

export default Sardines;