import React, { Component } from 'react';
const imgSrc = "https://i5.walmartimages.com/asr/d9939d0f-6382-4a0d-97c1-d5444345899e_1.c22bb525689793e89a3525a65f5a730c.jpeg";

class Chips extends Component {
    constructor(props){
        super(props);
        this.state = {
            bags: []
        };
        this.handleAdd = this.handleAdd.bind(this);
    }
    handleAdd() {
        const x = window.innerWidth * Math.random();
        const y = window.innerHeight * Math.random();
        this.setState(prevState => ({
            bags: [...prevState.bags, {x, y}]
        }));
    }
    render() {
        const bags = this.state.bags.map((bag, i) => (
            <img 
                key={i}
                src={imgSrc}
                style={{position: 'fixed', top: `${bag.y}px`, left: `${bag.x}px`,width: '200px' }}
                alt="chips"
            />
        ));
        return (
            <div>
                <h1>Chips count: {this.state.bags.length}</h1>
                <button onClick={this.handleAdd}>Add</button>
                {bags}
            </div>
        )
    }
}

export default Chips;
