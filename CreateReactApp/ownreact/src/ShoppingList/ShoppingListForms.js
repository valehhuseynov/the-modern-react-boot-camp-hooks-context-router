import React, { Component } from 'react'

class ShoppingListForms extends Component {
    constructor(props) {
        super(props);
        this.state = { name: "", qyt: "" }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleSubmit(evt) {
        evt.preventDefault();
        this.props.addItem(this.state);
        this.setState({ name: "", qyt: "" });
    }
    handleChange(evt) {
        this.setState({
            [evt.target.name]: evt.target.value
        });
    }
    render() {
        return (
            <form onSubmit={this.handleSubmit}>

                <label htmlFor="name">Name: </label>
                <input 
                type="text"
                name="name"
                id="name"
                value={this.state.name}
                onChange={this.handleChange}
                />

                <label htmlFor="qyt">Quantity: </label>
                <input 
                type="text"
                name="qyt"
                id="qyt"
                value={this.state.qyt}
                onChange={this.handleChange}
                /> 

                <button>Add Item</button>
                
            </form>
        )
    }
}

export default ShoppingListForms;
