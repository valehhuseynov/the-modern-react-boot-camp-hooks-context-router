import React, { Component } from 'react'
import ShoppingListForm from './ShoppingListForms';
import uuid from 'uuid/v4'

class ShoppingList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [
                { name: "Milk", qyt: "2 gallons", id: uuid() },
                { name: "Bread", qyt: "2 loaves", id: uuid() }
            ]
        };
        // this.renderItems = this.renderItems.bind(this);
        this.addItem = this.addItem.bind(this);
    }
    addItem(item) {
        let newItem = { ...item , id: uuid() }
        this.setState(state => ({
            items: [...state.items, newItem]
        }));
    }
    renderItems() {
        return (
            <ul>
                {this.state.items.map(item => (
                    <li key={item.id}>
                        {item.name}:{item.qyt}
                    </li>
                ))}
            </ul>
        );
    }
    render() {
        return (
            <div>
                <h1>SHopping List</h1>
                {this.renderItems()}
                <ShoppingListForm addItem={this.addItem} />
            </div>
        )
    }
}

export default ShoppingList;
