import React, { createContext, useState } from "react";

export const LanguageTheme = createContext();

export function LanguageProvider(props) {
  const [language, setLanguage] = useState("spanish");
  const changeLanguage = e => setLanguage(e.target.value);
  return (
    <LanguageTheme.Provider value={{ language, changeLanguage }}>
      {props.children}
    </LanguageTheme.Provider>
  );
}
