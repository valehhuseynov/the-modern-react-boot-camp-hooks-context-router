import React from "react";
// import { Route, Switch, NavLink } from "react-router-dom";
// import TodoApp from "./ReactHooks/HooksProject/TodoApp";
import ContextApp from "./ReactContext/ContextApp";
import "./App.css";

function App() {
  return (
    <div className="App">
      <ContextApp />
    </div>
  );
}

export default App;
