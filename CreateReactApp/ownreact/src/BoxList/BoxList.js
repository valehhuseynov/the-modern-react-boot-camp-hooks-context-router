import React, { Component } from 'react'
import Box from './Box';
import NewBoxForm from './NewBoXForm';


class BoxList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            boxes: []
        };
        this.addBox = this.addBox.bind(this);
    }
    remove(id) {
        this.setState({
            boxes: this.state.boxes.filter(box => box.id !== id) 
        });
    }
    addBox(item) {
        // let newBox = {...item , id:uuid()}
        // this.setState(st => ({
        //     boxes: [...st.boxes, newBox]
        // }));
        this.setState({
            boxes: [...this.state.boxes, item]
        })
    }
    render() {
        const boxes = this.state.boxes.map(box => (
            <Box 
             key={box.id}
             id={box.id}
             width={box.width} 
             height={box.height} 
             color={box.color} 
             removeBox={() => this.remove(box.id)}
             />
        ));
        return (
            <div>
                <h1>Color Box Maker Thingy</h1>
                <NewBoxForm addBox={this.addBox} />
                {boxes}
            </div>
        )
    }
}

export default BoxList;
