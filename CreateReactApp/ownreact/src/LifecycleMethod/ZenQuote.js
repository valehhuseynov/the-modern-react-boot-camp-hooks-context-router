import React, { Component } from 'react'
import axios from 'axios';
import './ZenQuote.css';


class ZenQuote extends Component {
    constructor(props) {
        console.log('INSIDE CONSTRUCTOR');
        super(props);
        this.state = { quote: "", isLoaded: false };
    }
    componentDidMount() {
        console.log('INSIDE COMPONENT DID MOUNT');
        // load date
        axios.get("https://api.github.com/zen").then(response => {
            setTimeout(() => {
                this.setState({ quote: response.data, isLoaded: true });
            }, 3000);
        });
        // set state width that date
    }
    componentDidUpdate() {
        console.log('INSIDE COMPONENT DID UPDATE');
    }
    render() {
        console.log('INSIDE RENDER');
        return (
            <div>
                {this.state.isLoaded ? (
                    <div>
                        <h1>Always remember...</h1>
                        <p>{this.state.quote}</p>
                    </div>
                ) : (
                    <div className="loader" id="loader-1"></div>
                )}
                
                
            </div>
        )
    }
}


export default ZenQuote;