import React, { Component } from 'react'

class Meal extends Component {
    render() {
        const foodName = this.props.match.params.foodName;
        const drinkName = this.props.match.params.drinkName;
        const foodUrl = `https://source.unsplash.com/1600x900/?${foodName}`;
        const drinkUrl = `https://source.unsplash.com/1600x900/?${drinkName}`;
        return (
            <div className="Food">
                <h1>Meal {foodName}</h1>
                <img src={foodUrl}  alt={foodName}/>
                <h1>Meal {drinkName}</h1>
                <img src={drinkUrl}  alt={drinkName}/>
            </div>
        )
    }
}

export default Meal;
