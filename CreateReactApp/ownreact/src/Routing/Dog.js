import React, { Component } from 'react'

class Dog extends Component {
    render() {
        return (
            <div className="Dogs">
                <h1>DOG!!!</h1>
                <h3>This dog is named: {this.props.name} </h3>
                <img src="https://cdn.pixabay.com/photo/2016/12/13/05/15/puppy-1903313__340.jpg" alt="dog" />
            </div>
        )
    }
}

export default Dog;