import React from "react";
import useInputState from "./hooks/useInputState";
function SimpleFormInputHook() {
  const [email, updateEmail, resetEmail] = useInputState("");
  const [password, updatePassword, resetPassword] = useInputState("");
  return (
    <div>
      <h1>
        This value is: {email} && {password}
      </h1>
      <input type="text" value={email} onChange={updateEmail} />
      <input type="text" value={password} onChange={updatePassword} />
      <button onClick={resetEmail}>Submit</button>
      <button onClick={resetPassword}>Submit Password</button>
    </div>
  );
}

export default SimpleFormInputHook;
