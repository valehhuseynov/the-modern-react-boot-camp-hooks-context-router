import React from "react";
import TodoApp from "./HooksProject/TodoApp";

function ReactHooksApp() {
  return <TodoApp />;
}

export default ReactHooksApp;
