import React, { Component } from 'react'
import Box from './Box';
class BoxContainer extends Component {
    static defaultProps = {
        numBox: 20,
        allColors: ['purple', 'magenta', 'violet', 'pink', 'aqua', 'black', 'darkblue', 'lightblue', 'lightpurple' , 'yellow' , 'darkyellow', 'lightyellow', 'teal','red','blue']
    }
    render() {
        const boxes = Array.from({length: this.props.numBox}).map(() => <Box colors={this.props.allColors} />);
        return (
            <div className="BoxContainer">{boxes}</div>
        )
    }
}


export default BoxContainer;
