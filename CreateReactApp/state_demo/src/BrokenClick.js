import React, { Component } from 'react'

class BrokenClick extends Component {
    constructor(props) {
        super(props);
        this.state = {
            clicked: false,
            hover: false,
            leave: false
        };
        this.handleClick = this.handleClick.bind(this);
        // this.handleHover = this.handleHover.bind(this);
        // this.handleLeave = this.handleLeave.bind(this);
    }
    handleClick(e) {
        this.setState({
            clicked: true
        })
    }
    // handleHover(e) {
    //     this.setState({
    //         hover: true,
    //         leave: false
    //     })
    // }
    // handleLeave(e) {
    //     this.setState({
    //         hover: false,
    //         leave: true
    //     })
    // }
     render() {
        return (
            <div>
                <h1>{this.state.clicked ? "Clicked" : "No Click"}</h1>
                <h2>{this.state.hover ? "Hover" : "No Hover"}</h2>
                <h3>{this.state.leave ? "Leave" : "No Leave"}</h3>
                <button onClick = {this.handleClick}>Click Me!</button>
            </div>
        )
    }
}

export default BrokenClick;