import React from "react";
import ReactDOM from "react-dom";
import foods from "./fruits";
import {choice , remove} from "./helpers";


// Randomly draw a fruit from the array 
let fruit = choice(foods);
// Log the message "I'd like one RANDOMFRUIT, please."
console.log(`I'd like one ${fruit}, please.`);
// Log the message "Here you go: RANDOMFRUIT"
console.log(`Here you go: ${fruit}`);
// Log the message "Declicious! May I have another"
console.log(`Declicious! May I have another`);
// Remove the fruit from the  array of fruits
let remaining = remove(foods , fruit);
// log the message "I'm sorry,we're all out.We have FRUITSLEFT left"
console.log(`I'm sorry,we're all out.We have ${remaining} left`);

class Index extends React.Component {
    render() {
        return (
            <div>
                <p>{`- I'd like one ${fruit}, please.`}</p>
                <p>{`- Here you go: ${fruit}`}</p>
                <p>{'- Declicious! May I have another'}</p>
                <p>{`- I'm sorry,we're all out.We have ${remaining} left`}</p>
            </div>
        )
    }
}

ReactDOM.render(<Index /> , document.getElementById('root'));
