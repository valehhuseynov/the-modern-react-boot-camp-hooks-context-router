import axios from "axios";

const Post = ({ id, comments }) => {
  return (
    <div>
      <p>Comments for Post: {id}</p>
      {comments.map(com => (
        <Comment key={com.id} {...com} />
      ))}
    </div>
  );
};

Post.getInitialProps = async function(context) {
  const { id } = context.query;
  const res = await axios.get(
    `https://jsonplaceholder.typicode.com/comments?postId=${id}`
  );
  const { data } = res;
  console.log(data);
  return { id, comments: data };
};

const Comment = ({ email, body }) => {
  return (
    <div>
      <h5>{email}</h5>
      <p>{body}</p>
    </div>
  );
};

export default Post;
